using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static Action<int> loadScene;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        
    }

    private void OnEnable()
    {
        loadScene += LoadScene;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        loadScene -= LoadScene;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        Loading.showLoading?.Invoke(false);
    }

    private void LoadScene(int sceneIndex) {
        Loading.showLoading?.Invoke(true);
        SceneManager.LoadScene(sceneIndex);
    }
}
