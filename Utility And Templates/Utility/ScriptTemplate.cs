using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FM.Utility
{
	public class ScriptTemplate : MonoBehaviour {

		#region Public Variables

		#endregion

		#region Private Variables

		#endregion

		#region Delegates

		#endregion

		#region Unity Callbacks
		void Awake()
        {
            
        }
		#endregion

		#region Public Methods

		#endregion

		#region Private Methods

		#endregion

		#region Coroutines

		#endregion

		#region UGUI Callbacks

		#endregion

		#region Event Callbacks

		#endregion
	}
}