﻿using System.Collections;
using System.Reflection;
//using UMX.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//using FM.Managers;

namespace FM.Utility
{
    [AddComponentMenu("GameObject/UI/Custom Button")]
    public class CustomButton : Button
    {
        #region Public Variables
        public bool playAudio = true;
        public ButtonActionType actionType;
        #endregion

        #region Private Variables
        #endregion
        
        #region Unity Callbacks

        protected override void OnDestroy()
        {
            base.OnDestroy();
            #if UNITY_2019_2 || UNITY_2019_2_3
            if(Application.isPlaying)
                RemovePersistentListenerInternal();
            #endif
        }

        #endregion
        
        #region Private Fuctions

        private void RemovePersistentListenerInternal()
        {
            /*var methodInfo = UnityEventBase.GetValidMethodInfo(onClick, "RemovePersistentListener", new[] {typeof(int)});
            var count = onClick.GetPersistentEventCount();
            for (int i = 0; i < count; i++)
            {
                methodInfo.Invoke(onClick, new object[] {i});
            }*/
            
            var fieldInfo = typeof(UnityEventBase).GetField("m_PersistentCalls",BindingFlags.Instance | BindingFlags.NonPublic);
            if (fieldInfo != null)
            {
                fieldInfo.SetValue(onClick, null);
                //Debug.LogError($"Setting Null : {fieldInfo} On [{gameObject.name}]");
            }
        }

        #endregion

        #region Event
        #endregion

        #region Unity Callbacks

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            StartCoroutine(Transition(new Vector3(1.1f, 1.1f, 1.1f), 0.25f));
            if (playAudio)
            {
                //if(actionType == ButtonActionType.Forward)
                //    Manager.AudioManager.PlayButtonForwardAudio();
                //else
                //    Manager.AudioManager.PlayButtonBackwardsAudio();
            }
        }

        #endregion

        private  IEnumerator Transition (Vector3 newSize, float transitionTime)
        {
            float timer = 0;
            Vector3 startSize = transform.localScale;
            while(timer < transitionTime)
            {
                timer += Time.deltaTime;
                yield return null;
                transform.localScale = Vector3.Lerp(startSize, newSize, timer / transitionTime);
            }

            yield return null;
            transform.localScale = Vector3.one;
        }
    }

    public enum ButtonActionType
    {
        Forward,
        Back
    }
}