﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FM.Utils
{
    public class Utility : MonoBehaviour
    {
        #region PUBLIC_VARS
        #endregion

        #region PRIVATE_VARS
        private System.Random _random = new System.Random();

        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        public static GameObject Get2DTouchObject()
        {
            GameObject touchObject = null;
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Utility.GetPosition()), Vector2.zero);
            if (hit.transform != null)
                touchObject = hit.transform.gameObject;
            return touchObject;
        }
        public static GameObject Get2DTouchObject(Vector3 screenPosition, LayerMask layerMask)
        {
            GameObject touchObject = null;
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(screenPosition), Vector2.zero, layerMask);
            if (hit.transform != null)
                touchObject = hit.transform.gameObject;
            return touchObject;
        }
        public static List<GameObject> Get2DTouchObjects()
        {
            List<GameObject> touchObjects = new List<GameObject>();
            RaycastHit2D[] hit = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Utility.GetPosition()), Vector2.zero);
            if (hit.Length > 0)
            {
                foreach (var item in hit)
                {
                    touchObjects.Add(item.transform.gameObject);
                }
            }

            return touchObjects;
        }

        public static Vector3 GetTouchPosition(float depth)
        {
            Vector3 touchPosition = Vector3.zero;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_METRO
            touchPosition = Input.mousePosition;

#else
            if(Input.touchCount>0){
                touchPosition = Input.GetTouch(0).position;
            }
#endif
            touchPosition.z = depth;
            return Camera.main.ScreenToWorldPoint(touchPosition);
        }

        public static Vector3 GetPosition()
        {
            Vector3 touchPosition = Vector3.zero;
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_METRO
            touchPosition = Input.mousePosition;
#else
            if(Input.touchCount>0){
                touchPosition = Input.GetTouch(0).position;
            }
#endif
            return touchPosition;
        }


        public static RaycastHit IsHit(Vector3 position, Vector3 direction, float distance)
        {
            RaycastHit hit;
            Debug.DrawRay(position, direction * distance, Color.red);
            if (Physics.Raycast(position, direction, out hit, distance))
            {
                return hit;
            }
            return hit;
        }

        public static RaycastHit IsHit(Vector3 position, Vector3 direction, float distance, LayerMask layerMask)
        {
            Ray ray = Camera.main.ScreenPointToRay(position);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * distance, Color.red);
            if (Physics.Raycast(ray.origin, ray.direction, out hit, distance, layerMask))
            {
                return hit;
            }
            return hit;
        }

        public static RaycastHit GetHit(Vector3 screenPosition, float distance, LayerMask layerMask)
        {
            Ray ray = Camera.main.ScreenPointToRay(screenPosition);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * distance, Color.red);
            if (Physics.Raycast(ray.origin, ray.direction, out hit, distance, layerMask))
            {
                return hit;
            }
            return hit;
        }

        public static RaycastHit2D GetHit2D(Vector3 screenPosition, float distance, LayerMask layerMask)
        {
            Ray ray = Camera.main.ScreenPointToRay(screenPosition);
            Ray2D ray2D = new Ray2D((Vector2)ray.origin, (Vector2)ray.direction);
            Debug.DrawRay(ray2D.origin, Vector3.forward * distance, Color.red);
            RaycastHit2D hit = Physics2D.Raycast(ray2D.origin, ray2D.direction, distance, layerMask, 0.2f, 500f);
            if (hit.collider != null)
            {
                return hit;
            }
            return hit;
        }

        public static bool IsHit2D(Vector3 screenPosition, float distance)
        {
            Ray ray = Camera.main.ScreenPointToRay(screenPosition);
            Ray2D ray2D = new Ray2D((Vector2)ray.origin, (Vector2)ray.direction);
            //Debug.DrawRay(ray2D.origin, Vector3.forward * distance, Color.red);
            RaycastHit2D hit = Physics2D.Raycast(ray2D.origin, ray2D.direction, distance);
            if (hit.collider != null)
            {
                return true;
            }
            return false;
        }

        public static Collider[] GetOverlapSphere(Vector3 position, float radius)
        {
            return Physics.OverlapSphere(position, radius);
        }

        public static Collider[] GetOverlapSphere(Vector3 position, float radius, LayerMask layerMask)
        {
            return Physics.OverlapSphere(position, radius, layerMask);
        }

        public static RaycastHit GetLineCast(Vector3 start, Vector3 end)
        {
            RaycastHit hit;
            Debug.DrawLine(start, end, Color.red);
            if (Physics.Linecast(start, end, out hit))
                return hit;
            return hit;
        }

        public static RaycastHit GetLineCast(Vector3 start, Vector3 end, LayerMask layerMask)
        {
            RaycastHit hit;
            Debug.DrawLine(start, end, Color.red);
            if (Physics.Linecast(start, end, out hit, layerMask))
                return hit;
            return hit;
        }

        public static bool GetTouchState()
        {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_METRO
            if (Input.GetMouseButtonDown(0))
                return true;
            if (Input.GetMouseButton(0))
                return true;
            else if (Input.GetMouseButtonUp(0))
                return false;

#else
        if(Input.touchCount == 1){
		    Touch touch = Input.GetTouch(0);
		    if(touch.phase == TouchPhase.Began)
			    return true;
		    else if(touch.phase == TouchPhase.Moved)
			    return true;
		    else if(touch.phase == TouchPhase.Stationary)
			    return true;
		    else if(touch.phase == TouchPhase.Canceled)
			    return false;
		    else if(touch.phase == TouchPhase.Ended)
			    return false;
            }
#endif
            return false;
        }

        public static Vector3 GetDirection(Vector3 firstPoint, Vector3 secondPoint)
        {
            Vector3 theRetDirection = Vector3.zero;
            theRetDirection = firstPoint - secondPoint;

            theRetDirection.z = 0;

            theRetDirection.Normalize();
            return theRetDirection;
        }

        public static string getPath()
        {
#if UNITY_EDITOR
            return Application.dataPath + "/Resources/";
#elif UNITY_ANDROID
    return Application.persistentDataPath;
#elif UNITY_IPHONE
    return GetiPhoneDocumentsPath()+"/";
#else
    return Application.dataPath +"/";
#endif
        }


        public static void InstantiatePrefab(GameObject prefab, Transform trans, out GameObject obj)
        {
            obj = (GameObject)Instantiate(prefab);
            obj.transform.SetParent(trans);
            obj.transform.localScale = Vector3.one;
            obj.transform.localPosition = Vector3.zero;
        }
        public static void LoadActiveScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }


        //for shuffle number from array
        public static void Shuffle(int[] array)
        {
            int p = array.Length;
            for (int n = p - 1; n > 0; n--)
            {
                int r = Random.Range(1, n);

                int t = array[r];
                array[r] = array[n];
                array[n] = t;
            }
        }

        //For shuffle + Random number from list
        public static List<int> GetRandomShuffleArray(int limit, int numOfElements, bool startFrom0 = true)
        {
            List<int> numbers = new List<int>();
            int startVal = startFrom0 ? 0 : 1;
            int endVal = startFrom0 ? limit : limit + 1;
            for (int i = startVal; i < endVal; i++)
            {
                numbers.Add(i);
            }

            List<int> shuffleNumbers = new List<int>();
            for (int i = 0; i < numOfElements; i++)
            {
                int randIndex = Random.Range(0, numbers.Count);
                int randValue = numbers[randIndex];
                shuffleNumbers.Add(randValue);
                numbers.Remove(randValue);
            }

            return shuffleNumbers;
        }


        #endregion

        #region PRIVATE_FUNCTIONS
        private static string GetiPhoneDocumentsPath()
        {
            // Strip "/Data" from path 
            string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
            // Strip application name 
            path = path.Substring(0, path.LastIndexOf('/'));
            return path + "/Documents";
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion
    }
}
