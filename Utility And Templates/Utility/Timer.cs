using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
//using FM.Utils;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    //public float timeInMinutes;
    public TMP_Text timerText;
    private float timeInSeconds;

    private void OnEnable()
    {
        timeInSeconds = 300; // DataManager.OrderTime;
        StartCoroutine("StartTimer");
        print(timeInSeconds);
    }

    private void OnDisable()
    {
        //Sound.instance.StopLooping();
    }

    private void OnDestroy()
    {
        //Sound.instance.StopLooping();
    }

    private void Update()
    {
        //timeInSeconds -= Time.deltaTime;
        //if (timeInSeconds <= 0)
        //{
        //    Debug.Log("Delayed order");
        //}
        //timerText.text = FormatTime();
    }

    IEnumerator StartTimer()
    {
        timeInSeconds--;
        timerText.text = FormatTime();
        while (timeInSeconds > 0)
        {
            yield return new WaitForSeconds(1);
            if(timeInSeconds <= 6)
            {
                timerText.color = Color.red;
                timerText.gameObject.transform.localScale = Vector3.one;
                timerText.gameObject.transform.DOScale(1.1f, 0.5f);
            }

            timeInSeconds--;
            timerText.text = FormatTime();
            //if (timeInSeconds <= 700)
            //    Sound.instance.PlayLooping(Sound.instance.TicTicClip);
        }
        //Sound.instance.StopLooping();
        UiController.instance.LevelFailed();
        yield return 0;
    }

    private string FormatTime()
    {
        int minutes = Mathf.FloorToInt(timeInSeconds / 60);
        int seconds = Mathf.FloorToInt(timeInSeconds % 60);
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    private void PlayCountDown()
    {

    }


    public float GetRemainingTime()
    {
        return timeInSeconds;
    }
}
