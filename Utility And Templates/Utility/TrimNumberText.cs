﻿using System.Collections;
using System.Collections.Generic;
using FM.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrimNumberText : MonoBehaviour {

    [SerializeField] TMP_Text itemText;

    private void Awake()
    {
        itemText = GetComponent<TMP_Text>();
    }

    private void Start()
    {
        UpdateBalance();
        CurrencyManager.onBalanceChanged += OnBalanceChanged;
    }

    private void UpdateBalance()
    {
        itemText.text = DataManager.Coins.ToString().FormateNumber();
        //UpdateText();
    }

    private void OnBalanceChanged()
    {
        UpdateBalance();
    }

    private void OnDestroy()
    {
        CurrencyManager.onBalanceChanged -= OnBalanceChanged;
    }

    public void UpdateText1()
    {
        float number;
        bool success = float.TryParse(itemText.text.Trim(), out number);
        if(success)
        {
            if (number >= 1000000000)
            {
                itemText.text = (number / 1000000000).ToString("0.0") + "B";
            }
            else if(number >= 1000000)
            {
                itemText.text = (number / 1000000).ToString("0.0") + "M";
            }
            else if (number >= 1000)
            {
                itemText.text = (number / 1000).ToString("0.0") + "K";
            }
        }
    }

    //public override string text
    //{
    //    get
    //    {
    //        return base.text;
    //    }
    //    set
    //    {
    //        base.text = value;
    //        UpdateText();
    //    }
    //}
}
