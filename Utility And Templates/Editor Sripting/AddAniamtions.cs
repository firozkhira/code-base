﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
#if UNITY_EDITOR
//using Unity.Transforms;
#endif
using UnityEditor;
using UnityEngine;

public class AddAniamtions : MonoBehaviour
{
    
#if UNITY_EDITOR

    [MenuItem("FM/ScaleAniamtion/Set Scale with 0 delay")]
    static void SetScale_0()
    {
        SetScaleProperty(0);
    }
    [MenuItem("FM/ScaleAniamtion/Set Scale with 0.1 delay")]
    static void SetScale_1()
    {
        SetScaleProperty(0.1f);
    }
    [MenuItem("FM/ScaleAniamtion/Set Scale with 0.2delay")]
    static void SetScale_2()
    {
        SetScaleProperty(0.2f);
    }
    [MenuItem("FM/ScaleAniamtion/Set Scale with 0.3 delay")]
    static void SetScale_3()
    {
        SetScaleProperty(0.3f);
    }
    
    static void SetScaleProperty(float delay)
    {
        GameObject obj = Selection.activeGameObject;
        obj.AddComponent<DOTweenAnimation>();
        DOTweenAnimation tweenAnimation = obj.GetComponent<DOTweenAnimation>();
        tweenAnimation.animationType = DOTweenAnimation.AnimationType.Scale;
        tweenAnimation.duration = 0.4f;
        tweenAnimation.easeType = Ease.OutBack;
        tweenAnimation.delay = delay;
        tweenAnimation.autoKill = false;
    
        obj.AddComponent<DOTweenVisualManager>();
        DOTweenVisualManager visaulManager = obj.GetComponent<DOTweenVisualManager>();
        visaulManager.onEnableBehaviour = OnEnableBehaviour.Play;
        visaulManager.onDisableBehaviour = OnDisableBehaviour.Rewind;
        obj.transform.localScale = Vector3.zero;
    }
    
    
    [MenuItem("FM/Animations/Set TopBar")]
    static void SetTopBar()
    {
        SetMoveProperty(Direction.Top, 200);
    }
    
    
    [MenuItem("FM/Animations/Set Bottom")]
    static void SetBottom()
    {
        SetMoveProperty(Direction.Bottom, 400);
    }
    
    [MenuItem("FM/Animations/Set Left Animation At 500")]
    static void SetLeftAniamtion_500()
    {
        SetMoveProperty(Direction.Left, 500);
    }
    
    [MenuItem("FM/Animations/Set Right Animation At 500")]
    static void SetRightAniamtion_500()
    {
       SetMoveProperty(Direction.Right, 500);
    }
    
    [MenuItem("FM/Animations/Set Left Animation At 650")]
    static void SetLeftAniamtion_650()
    {
        SetMoveProperty(Direction.Left, 650);
    }
    
    [MenuItem("FM/Animations/Set Right Animation At 650")]
    static void SetRightAniamtion_650()
    {
        SetMoveProperty(Direction.Right, 650);
    }
    
    [MenuItem("FM/Animations/Set Left Animation At 1250")]
    static void SetLeftAniamtion_1250()
    {
        SetMoveProperty(Direction.Left, 1250);
    }
    
    [MenuItem("FM/Animations/Set Right Animation At 1250")]
    static void SetRightAniamtion_1250()
    {
        SetMoveProperty(Direction.Right, 1250);
    }

    static void SetMoveProperty(Direction direction, int distance)
    {
        GameObject obj = Selection.activeGameObject;
        obj.AddComponent<DOTweenAnimation>();
        DOTweenAnimation tweenAnimation = obj.GetComponent<DOTweenAnimation>();
        tweenAnimation.animationType = DOTweenAnimation.AnimationType.Move;
        tweenAnimation.duration = 0.5f;
        tweenAnimation.easeType = Ease.OutCirc;
        tweenAnimation.autoKill = false;

        
        
        obj.AddComponent<DOTweenVisualManager>();
        DOTweenVisualManager visaulManager = obj.GetComponent<DOTweenVisualManager>();
        visaulManager.onEnableBehaviour = OnEnableBehaviour.Play;
        visaulManager.onDisableBehaviour = OnDisableBehaviour.Rewind;

        switch (direction)
        {
            case Direction.Left:
                obj.GetComponent<RectTransform>().anchoredPosition = Vector3.left * distance;
                break;
            
            case Direction.Right:
                obj.GetComponent<RectTransform>().anchoredPosition = Vector3.right * distance;
                break;
            
            case Direction.Top:
                obj.GetComponent<RectTransform>().anchoredPosition = Vector3.up * distance;
                break;
            
            case Direction.Bottom:
                obj.GetComponent<RectTransform>().anchoredPosition = Vector3.down * distance;
                break;
                    
        }
    }
    
      [MenuItem("FM/Animations/Set Fade Animation")]
        static void SetFadeAniamtion()
        {
            GameObject obj = Selection.activeGameObject;
            obj.AddComponent<DOTweenAnimation>();
            DOTweenAnimation tweenAnimation = obj.GetComponent<DOTweenAnimation>();
            tweenAnimation.animationType = DOTweenAnimation.AnimationType.Fade;
            tweenAnimation.duration = 0.4f;
            tweenAnimation.easeType = Ease.OutCirc;
            tweenAnimation.autoKill = false;

            obj.AddComponent<DOTweenVisualManager>();
            DOTweenVisualManager visaulManager = obj.GetComponent<DOTweenVisualManager>();
            visaulManager.onEnableBehaviour = OnEnableBehaviour.Play;
            visaulManager.onDisableBehaviour = OnDisableBehaviour.Rewind;

            obj.AddComponent<CanvasGroup>();
            obj.GetComponent<CanvasGroup>().alpha = 0;
        }

    enum Direction
    {
        Left,
        Right,
        Top,
        Bottom
    }
#endif

}


