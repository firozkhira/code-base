﻿using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;

namespace FM.Utils
{
    public static class ExtensionGameObject
    {
        public static void ActivateGameObject(this GameObject obj)
        {
            obj.SetActive(true);
        }
        public static void DeactivateGameObject(this GameObject obj)
        {
            obj.SetActive(false);
        }

        public static bool IsEnoughCoins(this ObscuredInt coins, ObscuredInt requiredCoins)
        {
            return coins >= requiredCoins;
        }
        
    }
}
