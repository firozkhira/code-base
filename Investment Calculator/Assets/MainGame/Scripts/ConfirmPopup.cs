using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmPopup : GeneralPopup
{
    Action<bool> confirmAaction;
    public void InitPopup(POPUP_TYPE type, string title, string description, Action<bool> action)
    {
        tmpTitieText.text = title;
        tmpDescText.text = description;
        this.confirmAaction = action;
    }

   public void OnYes()
    {
        gameObject.SetActive(false);
        confirmAaction(true);
    }

    public void OnNO()
    {
        gameObject.SetActive(false);
        confirmAaction(false);
    }
}
