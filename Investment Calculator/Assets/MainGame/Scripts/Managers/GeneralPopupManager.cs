﻿using System;
using UnityEngine;

namespace FM.Utils
{
    public class GeneralPopupManager : Singleton<GeneralPopupManager>
    {
        public GeneralPopup Popup;
        public ConfirmPopup confirmPopup;

		public void ActivatePopup(POPUP_TYPE type, string title, string description, Action action)
        {
            Popup.gameObject.SetActive(true);
            Popup.InitPopup(type, title, description, action);
        }


        public void ShowInfoPopup(string title, string description)
        {
            ActivatePopup(POPUP_TYPE.INFO, title, description, null);
        }

        public void ShowConfirmPopup(string title, string description, Action<bool> action)
        {
            confirmPopup.gameObject.SetActive(true);
            confirmPopup.InitPopup(POPUP_TYPE.INFO, title, description, action);
        }
    }
}
