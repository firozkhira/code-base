using FM.Utils;
using UnityEngine;
using UnityEngine.UI;

public class PanelManager : Singleton<PanelManager>
{
    public GameObject[] panels;
    public Button skipButton;
    public Button nextButton;

    public GameObject MainPanel;
    public GameObject gameCompletePanel;

    private int currentPanelIndex = 0;

    private void Start()
    {
        if (DataManager.IsIntroAppeared)
        {
            MainPanel.SetActive(true);
        }
        else
        {
            // Set the initial panel visibility
            panels[0].transform.parent.gameObject.SetActive(true);
            ShowCurrentPanel();

            // Attach button click events
            skipButton.onClick.AddListener(SkipToNextPanel);
            nextButton.onClick.AddListener(NextPanel);
        }
    }

    private void SkipToNextPanel()
    {
        // Disable the current panel
        panels[currentPanelIndex].SetActive(false);
        panels[currentPanelIndex].transform.parent.gameObject.SetActive(false);
        ShowFIrstPopup();

        //// Increment the current panel index
        //currentPanelIndex++;

        //// Check if the current panel index is out of range
        //if (currentPanelIndex >= panels.Length)
        //{
        //    // Handle when all panels have been shown (e.g., return to the first panel or perform another action)
        //    currentPanelIndex = 0;
        //}

        //// Enable the next panel
        //ShowCurrentPanel();
    }

    private void NextPanel()
    {
       
            // Disable the current panel
            panels[currentPanelIndex].SetActive(false);

        // Increment the current panel index
        currentPanelIndex++;

        print(currentPanelIndex + "    " + panels.Length);
        if (currentPanelIndex >= panels.Length)
        {
            panels[0].transform.parent.gameObject.SetActive(false);
            ShowFIrstPopup();
            return;
        }
        // Check if the current panel index is out of range
        if (currentPanelIndex >= panels.Length)
        {
            // Disable or hide the next button to indicate the end of the sequence
            //nextButton.gameObject.SetActive(false);
        }
        else
        {
            // Enable the next panel
            ShowCurrentPanel();
        }
    }

    private void ShowCurrentPanel()
    {
        // Enable the current panel
        DisableAllPanels();
        panels[currentPanelIndex].SetActive(true);
    }

    private void DisableAllPanels()
    {
        // Disable all panels
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }
    }

    private void ShowFIrstPopup()
    {
        GeneralPopupManager.Instance.ActivatePopup(POPUP_TYPE.CLAIM, "Montly Income", string.Format("You have recieved {0} Monthly income", Globals.InitialAmount.ToString().FormateIntWithSymbolRS()), () => {
            MainPanel.SetActive(true);
            DataManager.IsIntroAppeared = true;
        });
    }


    public void ShowGameCompleted()
    {
        gameCompletePanel.SetActive(true);
    }
}
