﻿using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public enum InvestmentType
{
    Stock,
    FD,
    RealEstate
}

namespace FM.Utils
{
    public class InvestmentManager : Singleton<InvestmentManager>
    {
        private float monthlyIncome;
        private float totalInvestment;
        private float currentValue;


        public TextMeshProUGUI monthlyIncomeText;
        public TextMeshProUGUI totalInvestmentText;
        public TextMeshProUGUI currentValueText;
        public GameObject InvestmentModule;
        public MoveAndPopup moveAndPopup;
        public InvestmentData investmentData;

        public Action OnInvestCompleted;
        public Action OnPullCompleted;


        public float MonthlyIncome
        {
            get { return monthlyIncome; }
            set { monthlyIncome = value;
                monthlyIncomeText.text = monthlyIncome.ToString().FormateIntWithSymbolRS();
            }
        }

        public float TotalInvestment
        {
            get { return totalInvestment; }
            set { totalInvestment = value;
                totalInvestmentText.text = totalInvestment.ToString().FormateIntWithSymbolRS();
            }
        }

        public float CurrentValue
        {
            get { return currentValue; }
            set { currentValue = value;
                currentValueText.text = currentValue.ToString().FormateIntWithSymbolRS();
            }
        }

        public InvestmentType SelectedInvestmentType;

        private void OnEnable()
        {
            Init();
           
        }


        void Init()
        {
            MonthlyIncome = DataManager.GetInHandCash();
            TotalInvestment = DataManager.GetTotalInvestment();
            CurrentValue = DataManager.GetTotalCurrentValue();
            if(DataManager.MyBaseYear == 0)
            {
                DataManager.MyBaseYear = Random.Range(1996, 2017);
            }
        }


        public void OnPlay()
        {
            // Perform the action only if TotalInvestment is greater than zero
            if (totalInvestment > 0)
            {
                // Add your logic for when the play event occurs
                moveAndPopup.OnPlay();
            }
            else
            {
                // Handle the case when TotalInvestment is not greater than zero
                GeneralPopupManager.Instance.ActivatePopup(POPUP_TYPE.INFO, "Warning!!", "You should invest first", () => { });
            }
        }

        public void OnPlus(int type)
        {
            // Add your logic for when the plus event occurs
            // This method takes an integer parameter for the value to add
            SelectedInvestmentType = (InvestmentType)type;
            InvestmentModule.SetActive(true);
        }

        public void OnMinus(int type)
        {
            // Add your logic for when the minus event occurs
            // This method takes an integer parameter for the value to subtract
            SelectedInvestmentType = (InvestmentType)type;
            InvestmentModule.SetActive(true);
        }


        public void OnInvest()
        {
            Init();
        }


        public void OnSell()
        {
            Init();
        }


        public void CalculateInvestmentReturn()
        {
            // currentValue = totalInvestment + (int)investmentReturn;


            // Return on FD
            var fdInvestment = DataManager.GetInvestedAmount(InvestmentType.FD);
            var yearIndex = (DataManager.MyBaseYear  - 1996);
            var fdRate = investmentData.GetReturnRate(InvestmentType.FD, yearIndex, DataManager.CurrentMonth);
            float fdInvestmentReturn = fdInvestment * (fdRate / 100.0f);// 0.08f; // 8% return
            var newAmount = (int)(fdInvestmentReturn / 12.0f);
            DataManager.UpdateCurrentValue( InvestmentType.FD, newAmount);

            // Return on Stocks
            var stocksInvestment = DataManager.GetCurrentValue(InvestmentType.Stock);
            var stockRate = investmentData.GetReturnRate(InvestmentType.Stock, yearIndex, DataManager.CurrentMonth);
            float stockInvestmentReturn = stocksInvestment * (stockRate / 100.0f);// 0.08f; // 8% return
            var newAmount1 = (int)(stockInvestmentReturn / 12.0f);
            DataManager.UpdateCurrentValue(InvestmentType.Stock, newAmount1);

            // Return on Real Esate
            var rsInvestment = DataManager.GetInvestedAmount(InvestmentType.RealEstate);
            var rsRate = investmentData.GetReturnRate(InvestmentType.RealEstate, yearIndex, DataManager.CurrentMonth);
            float rsInvestmentReturn = rsInvestment * (rsRate / 100.0f);// 0.08f; // 8% return
            var newAmount2 = (int)(rsInvestmentReturn / 12.0f);

            DataManager.UpdateCurrentValue(InvestmentType.RealEstate, newAmount2);

            Init();

        }

        
    }

}
