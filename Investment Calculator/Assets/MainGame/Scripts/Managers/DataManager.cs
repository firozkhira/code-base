using UnityEngine;

public static class DataManager
{
    private const string InvestedStocksKey = "InvestedStocks";
    private const string InvestedKey = "Invested";
    private const string InvestedFDKey = "InvestedFD";
    private const string InvestedRealEstateKey = "InvestedRealEstate";
    private const string InHandCashKey = "InHandCash";
    private const string CurrentValueKey = "CurrentValue";

    public static float GetTotalInvestment()
    {
        return GetInvestedAmount(InvestmentType.Stock) + GetInvestedAmount(InvestmentType.FD) + GetInvestedAmount(InvestmentType.RealEstate);
    }


    //public static void SetInvestedAmount(InvestmentType investmentType, float value)
    //{
    //    PlayerPrefs.SetFloat(InvestedKey +  investmentType, value);
    //}

    public static float GetInvestedAmount(InvestmentType investmentType)
    {
        return PlayerPrefs.GetFloat(InvestedKey + investmentType, 0);
    }

    public static void UpdateInvestedAmount(InvestmentType investmentType, float value)
    {
        PlayerPrefs.SetFloat(InvestedKey + investmentType,  (GetInvestedAmount(investmentType) + value));
        UpdateCurrentValue(investmentType, value);
    }



    public static void SetInHandCash(float value)
    {
        PlayerPrefs.SetFloat(InHandCashKey, value);
    }

    public static void UpdateInHandCash(int value)
    {
        PlayerPrefs.SetFloat(InHandCashKey, GetInHandCash() + value);
    }

    public static float GetInHandCash()
    {
        return PlayerPrefs.GetFloat(InHandCashKey, Globals.InitialAmount);
    }

    public static void SetCurrentValue(float value)
    {
        PlayerPrefs.SetFloat(CurrentValueKey, value);
    }

    public static float GetCurrentValue(InvestmentType investmentType)
    {
        return PlayerPrefs.GetFloat(CurrentValueKey + investmentType);
    }

    public static void UpdateCurrentValue(InvestmentType investmentType, float value)
    {
        PlayerPrefs.SetFloat(CurrentValueKey + investmentType, GetCurrentValue(investmentType) + value);
    }

    public static float GetTotalCurrentValue()
    {
        return GetCurrentValue(InvestmentType.Stock) + GetCurrentValue(InvestmentType.FD) + GetCurrentValue(InvestmentType.RealEstate);

    }


    private const string CurrentMonthKey = "CurrentMonth";
    private const string MyBaseYearKey = "MyBaseYear";
    private const string CurrentYearKey = "CurrentYear";
    private const string CurrentQuarterKey = "CurrentQuarter";
    private const string IsIntroAppearedKey = "IsIntroAppeared";

    public static int CurrentMonth
    {
        get { return PlayerPrefs.GetInt(CurrentMonthKey, 1); }
        set { PlayerPrefs.SetInt(CurrentMonthKey, value); }
    }

    public static int MyBaseYear
    {
        get { return PlayerPrefs.GetInt(MyBaseYearKey, 0); }
        set { PlayerPrefs.SetInt(MyBaseYearKey, value); }
    }

    public static int CurrentYear
    {
        get { return PlayerPrefs.GetInt(CurrentYearKey, 2021); }
        set { PlayerPrefs.SetInt(CurrentYearKey, value); }
    }

    public static int CurrentQuarter
    {
        get { return PlayerPrefs.GetInt(CurrentQuarterKey, 1); }
        set { PlayerPrefs.SetInt(CurrentQuarterKey, value); }
    }

    public static bool IsIntroAppeared
    {
        get { return PlayerPrefs.GetInt(IsIntroAppearedKey, 0) == 1; }
        set { PlayerPrefs.SetInt(IsIntroAppearedKey, value ? 1 : 0); }
    }
}
