using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameCompleted : MonoBehaviour
{
    public Image filledImage;
    public TMP_Text message;
    public Color failedColor;
    public Color successColor;


    private float totalTarget = 40000000;
    private float requiredTarget = 30000000;

    bool isAchived;
    // Start is called before the first frame update
    void OnEnable()
    {
        float myTarget =  (DataManager.GetInHandCash() + DataManager.GetTotalCurrentValue());
        if(myTarget < requiredTarget)
        {
            message.text = "Unfortunetly you were unable to make 3Cr in 5 years.";
            filledImage.color = failedColor;
        }
        else
        {
            message.text = "Congrats you were able to make 3Cr in 5 years.";
            filledImage.color = successColor;
        }

        filledImage.fillAmount = myTarget / totalTarget;
    }

}
