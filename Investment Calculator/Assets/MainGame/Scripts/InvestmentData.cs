using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvestmentData : MonoBehaviour
{
    public List<FDRData> FDData = new List<FDRData>();
    public List<EquityData> EQData = new List<EquityData>();
    public List<RealEstateData> RSData = new List<RealEstateData>();

    void Start()
    {
        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        FDData = new List<FDRData>();

        string FileString = ReadStreamingAssetsFile("fdrates.csv");

        string[] fdrs = FileString.Split('\n');

        for (int i = 0; i < fdrs.Length; i++)
        {
            string thisfdr = fdrs[i].TrimStart().TrimEnd();
            thisfdr = thisfdr.Replace("%", "");

            if (thisfdr.Length > 0)
            {
                string[] Items = thisfdr.Split(',');

                int year = 0;
                float base_equity = 0;
                float altered_equity = 0;
                float fixed_deposit = 0;

                if (Items.Length >= 1)
                {
                    year = int.Parse(Items[0]);
                }
                if (Items.Length >= 2)
                {
                    base_equity = float.Parse(Items[1]);
                }
                if (Items.Length >= 2)
                {
                    altered_equity = float.Parse(Items[2]);
                }
                if (Items.Length >= 2)
                {
                    fixed_deposit = float.Parse(Items[3]);
                }

                FDData.Add(new FDRData());
                FDData[FDData.Count - 1].year = year;
                FDData[FDData.Count - 1].base_equity = base_equity;
                FDData[FDData.Count - 1].altered_equity = altered_equity;
                FDData[FDData.Count - 1].fixed_deposit = fixed_deposit;

                yield return null;
            }
        }

        EQData = new List<EquityData>();

        FileString = ReadStreamingAssetsFile("equity.csv");

        string[] equities = FileString.Split('\n');

        for (int i = 0; i < equities.Length; i++)
        {
            string thiseq = equities[i].TrimStart().TrimEnd();
            thiseq = thiseq.Replace("%", "");

            if (thiseq.Length > 0)
            {
                string[] Items = thiseq.Split(',');

                int year = 0;
                float base_equity = 0;
                float altered_equity = 0;
                float average_equity = 0;
                float jan = 0;
                float feb = 0;
                float mar = 0;
                float apr = 0;
                float may = 0;
                float jun = 0;
                float jul = 0;
                float aug = 0;
                float sep = 0;
                float oct = 0;
                float nov = 0;
                float dec = 0;
                float high = 0;
                float low = 0;
                float other = 0;

                if (Items.Length >= 1)
                {
                    year = int.Parse(Items[0]);
                }
                if (Items.Length >= 2)
                {
                    base_equity = float.Parse(Items[1]);
                }
                if (Items.Length >= 3)
                {
                    altered_equity = float.Parse(Items[2]);
                }
                if (Items.Length >= 4)
                {
                    jan = float.Parse(Items[4]);
                }
                if (Items.Length >= 5)
                {
                    feb = float.Parse(Items[5]);
                }
                if (Items.Length >= 6)
                {
                    mar = float.Parse(Items[6]);
                }
                if (Items.Length >= 7)
                {
                    apr = float.Parse(Items[7]);
                }
                if (Items.Length >= 8)
                {
                    may = float.Parse(Items[8]);
                }
                if (Items.Length >= 9)
                {
                    jun = float.Parse(Items[9]);
                }
                if (Items.Length >= 10)
                {
                    jul = float.Parse(Items[10]);
                }
                if (Items.Length >= 11)
                {
                    aug = float.Parse(Items[11]);
                }
                if (Items.Length >= 12)
                {
                    sep = float.Parse(Items[12]);
                }
                if (Items.Length >= 13)
                {
                    oct = float.Parse(Items[13]);
                }
                if (Items.Length >= 14)
                {
                    nov = float.Parse(Items[14]);
                }
                if (Items.Length >= 15)
                {
                    dec = float.Parse(Items[15]);
                }
                if (Items.Length >= 16)
                {
                    other = float.Parse(Items[16]);
                }

                EQData.Add(new EquityData());
                EQData[EQData.Count - 1].year = year;
                EQData[EQData.Count - 1].base_equity = base_equity;
                EQData[EQData.Count - 1].altered_equity = altered_equity;
                EQData[EQData.Count - 1].average_equity = average_equity;
                EQData[EQData.Count - 1].jan = jan;
                EQData[EQData.Count - 1].feb = feb;
                EQData[EQData.Count - 1].mar = mar;
                EQData[EQData.Count - 1].apr = apr;
                EQData[EQData.Count - 1].may = may;
                EQData[EQData.Count - 1].jun = jun;
                EQData[EQData.Count - 1].jul = jul;
                EQData[EQData.Count - 1].aug = aug;
                EQData[EQData.Count - 1].sep = sep;
                EQData[EQData.Count - 1].oct = oct;
                EQData[EQData.Count - 1].nov = nov;
                EQData[EQData.Count - 1].dec = dec;
                EQData[EQData.Count - 1].other = other;

                yield return null;
            }
        }

        RSData = new List<RealEstateData>();

        FileString = ReadStreamingAssetsFile("realestates.csv");

        string[] realestates = FileString.Split('\n');

        for (int i = 0; i < realestates.Length; i++)
        {
            string thiseq = realestates[i].TrimStart().TrimEnd();
            thiseq = thiseq.Replace("%", "");

            if (thiseq.Length > 0)
            {
                string[] Items = thiseq.Split(',');

                int year = 0;
                float jan = 0;
                float feb = 0;
                float mar = 0;
                float apr = 0;
                float may = 0;
                float jun = 0;
                float jul = 0;
                float aug = 0;
                float sep = 0;
                float oct = 0;
                float nov = 0;
                float dec = 0;
                float other = 0;

                if (Items.Length >= 1)
                {
                    year = int.Parse(Items[0]);
                }
                if (Items.Length >= 2)
                {
                    jan = float.Parse(Items[1]);
                }
                if (Items.Length >= 3)
                {
                    feb = float.Parse(Items[2]);
                }
                if (Items.Length >= 4)
                {
                    mar = float.Parse(Items[3]);
                }
                if (Items.Length >= 5)
                {
                    apr = float.Parse(Items[4]);
                }
                if (Items.Length >= 6)
                {
                    may = float.Parse(Items[5]);
                }
                if (Items.Length >= 7)
                {
                    jun = float.Parse(Items[6]);
                }
                if (Items.Length >= 8)
                {
                    jul = float.Parse(Items[7]);
                }
                if (Items.Length >= 9)
                {
                    aug = float.Parse(Items[8]);
                }
                if (Items.Length >= 10)
                {
                    sep = float.Parse(Items[9]);
                }
                if (Items.Length >= 11)
                {
                    oct = float.Parse(Items[10]);
                }
                if (Items.Length >= 12)
                {
                    nov = float.Parse(Items[11]);
                }
                if (Items.Length >= 13)
                {
                    dec = float.Parse(Items[12]);
                }
                if (Items.Length >= 14)
                {
                    other = float.Parse(Items[13]);
                }

                RSData.Add(new RealEstateData());
                RSData[RSData.Count - 1].year = year;
                RSData[RSData.Count - 1].jan = jan;
                RSData[RSData.Count - 1].feb = feb;
                RSData[RSData.Count - 1].mar = mar;
                RSData[RSData.Count - 1].apr = apr;
                RSData[RSData.Count - 1].may = may;
                RSData[RSData.Count - 1].jun = jun;
                RSData[RSData.Count - 1].jul = jul;
                RSData[RSData.Count - 1].aug = aug;
                RSData[RSData.Count - 1].sep = sep;
                RSData[RSData.Count - 1].oct = oct;
                RSData[RSData.Count - 1].nov = nov;
                RSData[RSData.Count - 1].dec = dec;
                RSData[RSData.Count - 1].average = other;

                yield return null;
            }
        }
    }

    public string ReadStreamingAssetsFile(string File)
    {
        string FilePath = System.IO.Path.Combine(Application.streamingAssetsPath, File);
        string ReadString = System.IO.File.ReadAllText(FilePath);
        return ReadString;
    }

    public float GetReturnRate(InvestmentType type, int year, int day)
    {
        if (type == InvestmentType.Stock)
            return GetMonthReturn(EQData[year], day);

        if (type == InvestmentType.FD)
            return FDData[year].fixed_deposit;

        if (type == InvestmentType.RealEstate)
            return GetMonthReturn(RSData[year], day);

        return 0;
    }

    private float GetMonthReturn(EquityData equityData,  int day)
    {
        float retValue = 0;

        switch (day)
        {
            case 1:
                retValue = equityData.jan;
                break;

            case 2:
                retValue = equityData.feb;
                break;

            case 3:
                retValue = equityData.mar;
                break;

            case 4:
                retValue = equityData.apr;
                break;

            case 5:
                retValue = equityData.may;
                break;

            case 6:
                retValue = equityData.jun;
                break;

            case 7:
                retValue = equityData.jul;
                break;

            case 8:
                retValue = equityData.aug;
                break;

            case 9:
                retValue = equityData.sep;
                break;

            case 10:
                retValue = equityData.oct;
                break;

            case 11:
                retValue = equityData.nov;
                break;

            case 12:
                retValue = equityData.dec;
                break;

        }

        return retValue;
    }

    private float GetMonthReturn(RealEstateData rsData, int day)
    {
        float retValue = 0;

        switch (day)
        {
            case 1:
                retValue = rsData.jan;
                break;

            case 2:
                retValue = rsData.feb;
                break;

            case 3:
                retValue = rsData.mar;
                break;

            case 4:
                retValue = rsData.apr;
                break;

            case 5:
                retValue = rsData.may;
                break;

            case 6:
                retValue = rsData.jun;
                break;

            case 7:
                retValue = rsData.jul;
                break;

            case 8:
                retValue = rsData.aug;
                break;

            case 9:
                retValue = rsData.sep;
                break;

            case 10:
                retValue = rsData.oct;
                break;

            case 11:
                retValue = rsData.nov;
                break;

            case 12:
                retValue = rsData.dec;
                break;

        }

        return retValue;
    }
}

[Serializable]
public class EquityData
{
    public int year;
    public float base_equity;
    public float altered_equity;
    public float average_equity;
    public float jan;
    public float feb;
    public float mar;
    public float apr;
    public float may;
    public float jun;
    public float jul;
    public float aug;
    public float sep;
    public float oct;
    public float nov;
    public float dec;
    public float high;
    public float low;
    public float other;
}

[Serializable]
public class RealEstateData
{
    public int year;
    public float jan;
    public float feb;
    public float mar;
    public float apr;
    public float may;
    public float jun;
    public float jul;
    public float aug;
    public float sep;
    public float oct;
    public float nov;
    public float dec;
    public float average;
}


[Serializable]
public class FDRData
{
    public int year;
    public float base_equity;
    public float altered_equity;
    public float fixed_deposit;
}
