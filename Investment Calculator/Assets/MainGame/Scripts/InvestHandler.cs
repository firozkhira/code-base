using UnityEngine;
using TMPro;
using FM.Utils;

public class InvestHandler : MonoBehaviour
{
    public TMP_Text investmentType;
    public TMP_Text totalInvestment;
    public TMP_InputField amountField;

    private void OnEnable()
    {
        float updatedTotalInvestment = DataManager.GetTotalInvestment();
        totalInvestment.text = updatedTotalInvestment.ToString();

        investmentType.text =  InvestmentManager.Instance.SelectedInvestmentType.ToString();
    }

    public void Invest()
    {
        InvestmentType selectedType = InvestmentManager.Instance.SelectedInvestmentType;
        float amount = float.Parse(amountField.text);

        if(amount <= 0)
        {
            GeneralPopupManager.Instance.ShowInfoPopup("Invalid Data", "Enter valid amount first");
            return;
        }
        float inHandCase = DataManager.GetInHandCash();
        // Check if in-hand balance is greater than or equal to the amount
        if (inHandCase >= amount)
        {
            float updatedTotalInvestment = DataManager.GetTotalInvestment() + amount;
            totalInvestment.text = updatedTotalInvestment.ToString();
            DataManager.UpdateInvestedAmount(InvestmentManager.Instance.SelectedInvestmentType, amount);
            DataManager.SetInHandCash(inHandCase - amount);
            InvestmentManager.Instance.OnInvest();
        }
        else
        {
            GeneralPopupManager.Instance.ShowInfoPopup("No Funds", "Insufficient funds for investment");
        }
    }

    public void Sell()
    {
        InvestmentType selectedType = InvestmentManager.Instance.SelectedInvestmentType;
        float amount = float.Parse(amountField.text);
        float inHandCase = DataManager.GetInHandCash();
        if (amount <= 0)
        {
            GeneralPopupManager.Instance.ShowInfoPopup("Invalid Data", "Enter valid amount first");
            return;
        }
        // Check if total investment is greater than or equal to the amount
        if (DataManager.GetTotalInvestment() >= amount)
        {
            // Perform the selling logic
            // Example: DataManager.Sell(selectedType, amount);

            float updatedTotalInvestment = DataManager.GetTotalInvestment() - amount;
            totalInvestment.text = updatedTotalInvestment.ToString();
            DataManager.UpdateInvestedAmount(InvestmentManager.Instance.SelectedInvestmentType, -amount);
            DataManager.SetInHandCash(inHandCase - amount);
            InvestmentManager.Instance.OnSell();
        }
        else
        {
            GeneralPopupManager.Instance.ShowInfoPopup("No Funds", "Insufficient investment to sell");
        }
    }

    public void ClosePopup()
    {
        // Close popup logic
    }
}
