﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using FM.Utils;
using UnityEngine;
using UnityEngine.UI;

public class GeneralPopup : MonoBehaviour
{
    public TMP_Text tmpTitieText;
    public TMP_Text tmpDescText;

    public Button closeButton;
    public TMP_Text buttonText; 

    Action actionShowMap;

    private void Start()
    {
        if(closeButton != null)
            closeButton.onClick.AddListener(ClosePopup);

    }

    private void OnDestroy()
    {
        if(closeButton != null)
            closeButton.onClick.RemoveListener(ClosePopup);
    }

    private void ClosePopup()
    {
        gameObject.SetActive(false);
        if(actionShowMap != null)
            actionShowMap.Invoke();
    }

    public void InitPopup(POPUP_TYPE type, string title, string description, Action actionShowMap)
    {
        tmpTitieText.text = title;
        tmpDescText.text = description;
        this.actionShowMap = actionShowMap;
        if (type == POPUP_TYPE.INFO)
            buttonText.text = "Close";
        else
            buttonText.text = "Claim";
    }

    public void OnShowLocation()
    {
        gameObject.SetActive(false);
        actionShowMap.Invoke();
    }
}

public enum POPUP_TYPE
{
    INFO,
    CLAIM
}
