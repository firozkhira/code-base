using FM.Utils;
using TMPro;
using UnityEngine;

public class MoveAndPopup : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float stopPercentage = 0.25f;
    public GameObject popupPrefab;
    public Vector3 startPoint;
    public Vector3 endPoint;
    public GameObject playButton;
    public GameObject handle;
    public TMP_Text yearText;

    private bool isMoving = true;
    private float quarerTargetPositionX;
    private float monthlyTargetPositionX;

    private bool isPopupShown;
    //private int numOfQuarter;
    //private int month;

    float[] monthPercentages = new float[] {0.0833f, 0.1666f, 0.25f, 0.3333f, 0.4166f, 0.5f,
                                            0.5833f, 0.6666f, 0.75f, 0.8333f, 0.9166f, 1};

    private void Start()
    {
        //month = ;
        // Calculate the target position based on the stop percentage
        Init();
        //numOfQuarter = 1;
        print(quarerTargetPositionX);
        isMoving = false;
    }

    void Init()
    {
        quarerTargetPositionX = Mathf.Lerp(startPoint.x, endPoint.x, stopPercentage * DataManager.CurrentQuarter);
        monthlyTargetPositionX = Mathf.Lerp(startPoint.x, endPoint.x, monthPercentages[DataManager.CurrentMonth - 1]);

        if (DataManager.CurrentMonth > 1)
        {
            var monthlyBasePositionX = Mathf.Lerp(startPoint.x, endPoint.x, monthPercentages[DataManager.CurrentMonth - 2]);
            var pos = handle.transform.localPosition;
            handle.transform.localPosition = new Vector3(monthlyBasePositionX, pos.y, pos.z);
        }
        else
        {
            handle.transform.localPosition = startPoint;
        }
        yearText.text = DataManager.CurrentYear.ToString();
    }

    

    public void OnPlay()
    {
        playButton.SetActive(false);
        isPopupShown = false;
        isMoving = true;
    }

    private void Update()
    {
        if (isMoving)
        {
            // Move the GameObject horizontally
            handle.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);

            if (handle.transform.localPosition.x >= monthlyTargetPositionX && !isPopupShown)
            {
                isMoving = false;
                SetNextMonthTarget();
            }
                // Check if the current position exceeds the target position and a popup is not shown yet
                if (handle.transform.localPosition.x >= quarerTargetPositionX && !isPopupShown)
            {
                ShowPopup();
                isPopupShown = true;
                isMoving = false;
            }
        }
    }

    private void ShowPopup()
    {
        // Instantiate the popup prefab at the current position
        //Instantiate(popupPrefab, transform.position, Quaternion.identity);
        print("Stop moving");
        GeneralPopupManager.Instance.ShowConfirmPopup("Update Investment", "Do you want to change your investment plan?", (state) =>
        {
            if (state)
            {
                playButton.SetActive(true);
                SetNextMovingTarget();
            }
            else
            {
                SetNextMovingTarget();
                isMoving = true;
                isPopupShown = false;
            }
        });
    }

    private void SetNextMovingTarget()
    {
        DataManager.CurrentQuarter++;
        if (DataManager.CurrentQuarter > 4)
        {
            DataManager.CurrentQuarter = 1;
            DataManager.CurrentMonth = 1;
            DataManager.CurrentYear++;
            DataManager.MyBaseYear++;
            if (DataManager.CurrentYear < 2026)
                Init();
            else
            {
                PanelManager.Instance.ShowGameCompleted();
                this.enabled = false;
            }
        }
        else
            quarerTargetPositionX = Mathf.Lerp(startPoint.x, endPoint.x, stopPercentage * DataManager.CurrentQuarter);
      
    }

    private void SetNextMonthTarget()
    {
        //month++;

        DataManager.CurrentMonth++;
        if (DataManager.CurrentMonth > 12)
        {
            //DataManager.CurrentMonth = 1;
            //DataManager.CurrentYear++;
            //DataManager.MyBaseYear++;
        }
        else
        {
            monthlyTargetPositionX = Mathf.Lerp(startPoint.x, endPoint.x, monthPercentages[DataManager.CurrentMonth - 1]);
            GiveMonthlyReturn();
            print(monthlyTargetPositionX);
        }

        //isMoving = true;
    }


    private void GiveMonthlyReturn()
    {
        DataManager.UpdateInHandCash(Globals.InitialAmount);
        InvestmentManager.Instance.CalculateInvestmentReturn();
        isMoving = true;
    }

}
