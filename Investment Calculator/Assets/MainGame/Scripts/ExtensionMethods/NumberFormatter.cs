﻿
using UnityEngine;

public static class NumberFormatter
{
    public static string FormateNumber(this string numString)
    {
        return string.Format("{0:n0}", int.Parse(numString));
    }

    public static string FormateIntWithSymbolRS(this string numString)
    {
        return string.Format("₹ {0:n0}", int.Parse(numString));
    }
    public static string FormateFloatNumber(this string numString)
    {
        return string.Format("{0:n2}", float.Parse(numString));
    }

    public static string FormateFloatNumberWithSymbolRS(this string numString)
    {
        return string.Format("₹ {0:n2}", float.Parse(numString));
    }

    public static string AddSuffixS(this string str, string suffixString)
    {
        int strVlaue = int.Parse(str);
        return string.Format("{0} {1}{2}", str, suffixString, strVlaue > 1 ? "s" : "");
    }

    public static string AddPerSymbol(this string str)
    {
        return string.Format("{0} %", str);
    }
    public static string AddPerSymbolToFLoat(this string str)
    {
        return string.Format("{0:n2} %", float.Parse(str));
    }

    public static string AddGramSymbol(this string str)
    {
        return string.Format("{0} g", str);
    }
    public static string GetStringWitColan(this string msg, string value)
    {
        return string.Format("{0} : {1}", msg, value);
    }

    public static string AddRS(this string str)
    {
        return string.Format("{0} RS", str);
    }

    public static string AddBullet(this string str)
    {
        return string.Format("● {0}", str);
    }

    public static string FormateQuantityWithUnit(this string quntString, string unit)
    {
        return string.Format("{0:n0} {1}", int.Parse(quntString), unit);
    }
}
